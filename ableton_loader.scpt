-- alter these two to turn on/off logging and change the file
-- if you choose to enable logging, you can use Console.app to read it in the ~/Library/Logs section
property logging : true as boolean
property logFile : (the POSIX path of (path to home folder)) & "Library/Logs/ableton_loader.log"

on runme (message)
    if (item 1 of message = 192) and (item 2 of message > 0) then
        set linksFile to (the POSIX path of (path to home folder)) & "Music/Ableton/links.txt"
        set links to read linksFile using delimiter linefeed
        
        -- exit if pch number is more than the number of lines in the file
        if (item 2 of message > (count of links)) then return
        
        set proj to item ((item 2 of message)) of links
        
        -- exit if there is something wrong with the input from the links file (not found, not a file path, etc)
        set found to false as boolean
        try
            tell application "Finder" to if exists proj as POSIX file then set found to true
        end try
        if (found is false) then
            log proj & " not found"
            return
        end if
        
        log "echo opening " & proj
        
        -- tell ableton to open the project file
        ignoring application responses
            tell application "Ableton Live 9 Suite"
                activate
                open proj
            end tell
        end ignoring
        
        -- wait 1 sec for the "save/don't save" box to pop up and generate  a cmd-D press.
        -- if the box doesn't pop up then cmd-D does nothing in ableton so no worries
        
        delay 1
        tell application "System Events"
            key code 2 using command down
        end tell
    end if
end runme

-- helper function for logging
on log message
    if (logging is true) then do shell script "echo " & message & " >> " & logFile
end log
