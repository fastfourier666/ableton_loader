# ableton_loader

applescript to switch ableton projects

## How to use

Download [MIDIPipe](http://www.subtlesoft.square7.net/MidiPipe.html) and paste the script file contents into an Applescript Trigger tool.

Edit the script file to enable/disable logging, and to target the correct version of Live (where it says "Ableton Live 9 Suite" or whatever)

Create the file ```~/Music/Ableton/links.txt``` with the full path to an ableton set file on each line. The line number == the program change number which will recall that set. The file is checked for existence before loading, so to disable a "patch" just preface it with "#" or somesuch.

In this example, PC#1, 2, 3 and 5 will open .als files and all others will do nothing:

```
/Users/poop/Music/Ableton/beat111bong Project/beatbong.als
/Users/poop/Music/Ableton/mm2 Project/mm2.als
/Users/poop/Music/Ableton/pitchbendfuckery Project/pitchbendfuckery.als
#/Users/poop/Music/Ableton/pitchbendfuckery Project/pitchbendfuckery.als <--IGNORED
/Users/poop/Music/Ableton/awardwinningsong Project/awardwinningsong.als
```

_To get the full path of the .als file in the clipboard, highlight the file in Finder and do option-cmd-C._


On receiving a program change (channel 1) whose number corresponds to a line in the text file with a valid path, the script will attempt to open it. If Live is not running, it will be opened with that file. If Live is running, the existing set should be closed and the new one opened.

One second after issuing the open command, the script will fire a cmd-D ("don't save") at Live to dismiss the dialigue box. If nothing changed in the set and the box doesn't appear, cmd-D does nothing in Live anyway and the new set will be loaded immediately.

May have to enable some accessibility permissions for MidiPipe (system prefs-> security) so it can generate the keypress.
